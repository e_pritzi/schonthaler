from django.db import models


# Create your models here.
class StartPage(models.Model):
    Description = models.TextField(default="Give a description")

    image1 = models.ImageField(upload_to='card_images', blank=True)
    TitleBox1 = models.CharField(max_length=50)
    DescriptionBox1 = models.CharField(max_length=300)
    image2 = models.ImageField(upload_to='card_images', blank=True)
    TitleBox2 = models.CharField(max_length=50)
    DescriptionBox2 = models.CharField(max_length=300)
    image3 = models.ImageField(upload_to='card_images', blank=True)
    TitleBox3 = models.CharField(max_length=50)
    DescriptionBox3 = models.CharField(max_length=300)


class ImageInCarousel(models.Model):
    ImageSource = models.ImageField(upload_to='mainimages', blank=True)
    caption = models.CharField(max_length=50, blank=True)
    alt = models.CharField(max_length=50, blank=True)
    show = models.BooleanField(default=True)
