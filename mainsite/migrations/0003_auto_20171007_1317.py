# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-07 13:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainsite', '0002_startpage_image'),
    ]

    operations = [
        migrations.RenameField(
            model_name='startpage',
            old_name='image',
            new_name='image1',
        ),
        migrations.AddField(
            model_name='startpage',
            name='image2',
            field=models.ImageField(blank=True, upload_to='card_images'),
        ),
        migrations.AddField(
            model_name='startpage',
            name='image3',
            field=models.ImageField(blank=True, upload_to='card_images'),
        ),
    ]
