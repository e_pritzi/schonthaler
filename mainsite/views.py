from django.shortcuts import render
from mainsite.models import StartPage, ImageInCarousel


# Create your views here.
def home(request):
    model = StartPage.objects.all()[0]
    images = ImageInCarousel.objects.filter(show=True)
    args = {'siteInfo': model, 'images': images}
    return render(request, 'mainsite/index.html', args)
