from django.contrib import admin
from mainsite.models import StartPage, ImageInCarousel


# Register your models here.
admin.site.register(StartPage)
admin.site.register(ImageInCarousel)
